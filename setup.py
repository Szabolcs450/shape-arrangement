import setuptools

with open("README.md", "r") as fh:
    long_description_by_readme = fh.read()

setuptools.setup(
    name="Shape-Arrangements-Application",
    author="Szabolcs Tóth",
    author_email="szabolcs450@gmail.com",
    description="Desktop application for calculating the possible arrangements of 3D shapes",
    long_description=long_description_by_readme,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Szabolcs450/shape-arrangement",
    packages=setuptools.find_namespace_packages(include=["shape_arrangements.*"]),
    scripts=[
        "bin/shape-arrangements"
    ],
    python_requires=">=3.8",
    version_format="{tag}.dev{commitcount}+{gitsha}",
    setup_requires=[
        "setuptools-git-version",
    ],
    install_requires=[
        "wxPython~=4.1",  # for GUI
    ],
    extras_require={
        "dev": [
            "pylama~=7.7",  # for code checking
            "autopep8~=1.5",  # for code formatting
            "rope~=0.18",  # for VsCode renaming (F2)
            # to build GUI (GUI designer) use wxGlade (no pip package):
            #  http://wxglade.sourceforge.net/docs/intro.html#installing-wxglade
            "ipython",  # powerfull interactive Python shell
        ],
        "test": [
            "pytest~=6.0",
            "pytest-cov~=2.10",
        ],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)
