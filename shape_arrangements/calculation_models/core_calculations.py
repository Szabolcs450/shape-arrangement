from .shape_class_definitions import Face


class CoreCalculator():
    """Do every calculation."""

    def __init__(self, input_data):
        self.mode = input_data['mode_number']
        self.list_of_face_strings = input_data['faces']
        self.list_of_face_objects = []
        self.face_counter = 1
        for idx in self.list_of_face_strings:
            self.list_of_face_objects.append(Face(idx, self.face_counter))
            self.face_counter = self.face_counter + 1

    def calculate(self, mode, data):
        """TODO."""
        if mode == 1:
            pass
        elif mode == 2:
            pass
        elif mode == 3:
            pass
        else:
            """Throw error."""
