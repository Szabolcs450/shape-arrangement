# Collect the definition of smaller classes here in order to
# keep the size of core_calculations.py managable.
import numpy as np


class Face():
    """Groups and handles data that belong to 2D faces of shapes."""

    def __init__(self, input_string, face_id):
        self.face_id = face_id
        self.list_of_vertices_as_vectors = []
        self.number_of_vertices = len(self.list_of_vertices_as_vectors)
        self.number_of_colours = 0
        self.list_of_colours = []
        for idx in input_string:
            if len(idx) == 3 and idx.isdigit():
                self.list_of_vertices_as_vectors.append(np.array([int(idx[0]), int(idx[1]), int(idx[2])]))
            elif len(idx) <= 2 and idx.isdigit():
                self.number_of_colours = int(idx)
            elif not idx.isdigit():
                self.list_of_colours.append(idx)

        self.vect_from_vert1_to_vert2 = self.list_of_vertices_as_vectors[1] - self.list_of_vertices_as_vectors[0]

    def equal(self, another_face):
        """TODO."""

        if self.number_of_vertices != another_face.number_of_vertices:
            return False
        else:
            for idx in range(0, (self.number_of_vertices-1)):
                another_face.a = another_face.list_of_vertices[idx]
                another_face.b = another_face.list_of_vertices[(idx + 1 % self.number_of_vertices)]
                another_face.c = another_face.list_of_vertices[(idx + 2 % self.number_of_vertices)]
                # self.calc_orient_pres_iso(vect1, vect2, vect3)

    def calc_orient_pres_iso(self, vect_a, vect_b, vect_c):
        """TODO."""
        self.vect_a = vect_a
        self.vect_b = vect_b
        self.vect_c = vect_c
        self.K

        # translation that moves vect_a to face1[0]
        self.T = self.vect_a - self.list_of_vertices_as_vectors[0]
        # move the triplet such that the first point corresponds to face1[0]'s
        self.vect_b = self.vect_b - self.T
        self.vect_c = self.vect_c - self.T
        self.v1 = self.vect_b - self.list_of_vertices_as_vectors[1]
        # if the triplets second member is already in place, we can skip this.
        if np.linalg.norm(self.v1 - self.vect_from_vert1_to_vert2) < 0.0001:
            self.K = (np.array([1, 0, 0], [0, 1, 0], [0, 0, 1]), np.array([0, 0, 0]))
        else:  # get the direction of the axis of rotation
            self.axis_dir1 = np.cross(self.v1, self.vect_from_vert1_to_vert2)
            self.axis_dir1 = self.axis1 / np.linalg.norm(self.axis1)  # Ux, Uy, Uz helyére, sin = gyök{1 - cos_angle^2}
        # cosine of the angle of the rotation
        self.cos_angle = np.dot(self.v1, self.vect_from_vert1_to_vert2)
        self.cos_angle = self.cos_angle / (np.linalg.norm(self.v1)*np.linalg.norm(self.vect_from_vert1_to_vert2))
        # temporary matrix for the rotation around the axis with direction axis1
        # 3d_rot_func(v, cos_angle)
        self.M
