import wx
from .base_frame_controller import BaseFrame
from .input_controller import InputCollectorPanel


class ShapeArrangementsApp:
    """Main class for Shape Arrangements application."""

    def __init__(self):
        # init base app
        self._app = wx.App(0)
        # init base frame
        self._base_frame = BaseFrame(None, wx.ID_ANY, "")

    def run(self):
        """Run application."""
        self._base_frame.Show()
        self._app.MainLoop()
