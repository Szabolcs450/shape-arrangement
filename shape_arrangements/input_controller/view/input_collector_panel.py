# -*- coding: UTF-8 -*-
#
# generated by wxGlade 1.1.0pre on Thu Sep  9 13:33:35 2021
#

import wx

# begin wxGlade: dependencies
# end wxGlade

# begin wxGlade: extracode
# end wxGlade


class GeneratedInputCollectorPanel(wx.Panel):
    def __init__(self, *args, **kwds):
        # begin wxGlade: GeneratedInputCollectorPanel.__init__
        kwds["style"] = kwds.get("style", 0) | wx.TAB_TRAVERSAL
        wx.Panel.__init__(self, *args, **kwds)
        self.SetSize((1506, 906))

        sizer_1 = wx.BoxSizer(wx.VERTICAL)

        sizer_1.Add((100, 150), 0, 0, 0)

        login_screen_title = wx.StaticText(self, wx.ID_ANY, "3D Shape Arrangement Calculator")
        login_screen_title.SetFont(wx.Font(35, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, 0, ""))
        sizer_1.Add(login_screen_title, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALL, 0)

        sizer_1.Add((100, 100), 0, 0, 0)

        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1.Add(sizer_2, 1, wx.EXPAND, 0)

        sizer_2.Add((20, 20), 1, 0, 0)

        self.inputButtonId = 1
        self.problem_1_button = wx.Button(self, self.inputButtonId, "button_1")
        sizer_2.Add(self.problem_1_button, 1, wx.EXPAND, 0)

        sizer_2.Add((20, 20), 1, 0, 0)

        sizer_1.Add((100, 20), 0, 0, 0)

        sizer_3 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1.Add(sizer_3, 1, wx.EXPAND, 0)

        sizer_3.Add((20, 20), 1, 0, 0)

        self.inputButtonId = 2
        self.problem_2_button = wx.Button(self, self.inputButtonId, "button_2")
        sizer_3.Add(self.problem_2_button, 1, wx.EXPAND, 0)

        sizer_3.Add((20, 20), 1, 0, 0)

        sizer_1.Add((100, 20), 0, 0, 0)

        sizer_4 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1.Add(sizer_4, 1, wx.EXPAND, 0)

        sizer_4.Add((20, 20), 1, 0, 0)

        self.inputButtonId = 3
        self.problem_3_button = wx.Button(self, self.inputButtonId, "button_3")
        sizer_4.Add(self.problem_3_button, 1, wx.EXPAND, 0)

        sizer_4.Add((20, 20), 1, 0, 0)

        sizer_1.Add((100, 40), 0, 0, 0)

        sizer_5 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1.Add(sizer_5, 1, wx.EXPAND, 0)

        sizer_5.Add((20, 20), 1, 0, 0)

        self.open_csv_button = wx.Button(self, wx.ID_ANY, "open import file")
        sizer_5.Add(self.open_csv_button, 1, wx.EXPAND, 0)

        sizer_5.Add((20, 20), 1, 0, 0)

        sizer_1.Add((20, 20), 4, 0, 0)

        self.SetSizer(sizer_1)

        self.Layout()

        self.Bind(wx.EVT_BUTTON, self.buttons_handler, self.problem_1_button)
        self.Bind(wx.EVT_BUTTON, self.buttons_handler, self.problem_2_button)
        self.Bind(wx.EVT_BUTTON, self.buttons_handler, self.problem_3_button)
        self.Bind(wx.EVT_BUTTON, self.csv_button_handler, self.open_csv_button)
        # end wxGlade

    def buttons_handler(self, event):  # wxGlade: GeneratedInputCollectorPanel.<event_handler>
        print("Event handler 'buttons_handler' not implemented!")
        event.Skip()

    def csv_button_handler(self, event):  # wxGlade: GeneratedInputCollectorPanel.<event_handler>
        print("Event handler 'csv_button_handler' not implemented!")
        event.Skip()

# end of class GeneratedInputCollectorPanel
