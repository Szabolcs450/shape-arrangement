import logging
import wx
import os
import numpy as np
from csv import reader
from .view import GeneratedInputCollectorPanel
from ..base_frame_controller import register
from ..calculation_models import CoreCalculator

logger = logging.getLogger(__name__)


@register("input_collector_panel", start_panel=True)
class InputCollectorPanel(GeneratedInputCollectorPanel):
    """TODO."""

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)
        self.open_csv_button.Disable()
        self.mode_number = 0
        self.list_of_faces = []
        self.dirname = os.getcwd()
        self.input_data = {}  # collect everything here from input choices and files

    def Show(self, **kwargs):
        """TODO."""
        super().Show()

    def buttons_handler(self, event: wx.Event):
        """TODO."""
        buttonID = event.GetEventObject().GetId()

        if buttonID == 1:
            if self.mode_number != 1:
                self.mode_number = 1
            else:
                self.mode_number = 0
            if self.problem_2_button.IsEnabled() and self.problem_3_button.IsEnabled():
                self.problem_2_button.Disable()
                self.problem_3_button.Disable()
            else:
                self.problem_2_button.Enable()
                self.problem_3_button.Enable()
        elif buttonID == 2:
            if self.mode_number != 2:
                self.mode_number = 2
            else:
                self.mode_number = 0
            if self.problem_1_button.IsEnabled() and self.problem_3_button.IsEnabled():
                self.problem_1_button.Disable()
                self.problem_3_button.Disable()
            else:
                self.problem_1_button.Enable()
                self.problem_3_button.Enable()
        elif buttonID == 3:
            if self.mode_number != 3:
                self.mode_number = 3
            else:
                self.mode_number = 0
            if self.problem_1_button.IsEnabled() and self.problem_2_button.IsEnabled():
                self.problem_1_button.Disable()
                self.problem_2_button.Disable()
            else:
                self.problem_1_button.Enable()
                self.problem_2_button.Enable()

        if self.open_csv_button.IsEnabled() is False:
            self.open_csv_button.Enable()
        else:
            self.open_csv_button.Disable()

    def csv_button_handler(self, event: wx.Event):
        """TODO."""

        dlg = wx.FileDialog(self, 'Choose a file', self.dirname, '', 'CSV files (*.csv)|*.csv|All files(*.*)|*.*', wx.FD_OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            self.input_data['mode_number'] = self.mode_number
            self.dir_name = dlg.GetDirectory()
            self.file_name = os.path.join(self.dir_name, dlg.GetFilename())
            self.file = open(self.file_name, 'r')

            with open(self.filename, 'r') as read_obj:
                csv_reader = reader(read_obj)
                for row in csv_reader:
                    self.list_of_faces.append(row)

            self.input_data['faces'] = self.list_of_faces

            # make a CoreCalculator instance

            self.calculator = CoreCalculator(self.input_data)
            # make it calculate and return with the result
            # pass it to the results panel, and activate that panel while hiding this
        else:
            raise ValueError(f"cannot open csv")
