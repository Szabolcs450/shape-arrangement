import logging
from collections import namedtuple
import wx
from .view import GeneratedBaseFrame

logger = logging.getLogger(__name__)
_PanelInfo = namedtuple("PanelInfo", "cls, start_panel")
registered_panels = dict()


def register(name, *, start_panel=False):
    """Register a new panel class.

    Args:
        name (str): name of panel
        start_panel (bool): if True then panel will be shown at start
    """
    def decorated_func(cls):
        global registered_panels
        # check whether panel has been registered
        if name in registered_panels:
            raise ValueError(f"The panel with name '{name}' has been already registered'!")
        for panel_info in registered_panels.values():
            if panel_info.cls is cls:
                raise ValueError(f"The panel '{cls.__name__}' has been already registered'!")
            if start_panel and panel_info.start_panel:
                raise ValueError(f"The panel '{panel_info.cls.__name__}' has been already registered as start-panel'!")
        # register new panel
        registered_panels[name] = _PanelInfo(cls, start_panel)
    return decorated_func


class BaseFrame(GeneratedBaseFrame):
    """Custom working for main frame of app."""

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)
        # add registered panels
        self._panels = dict()
        for panel_name, panel_info in registered_panels.items():
            new_panel = panel_info.cls(self, wx.ID_ANY, name=panel_name)
            self.GetSizer().Add(new_panel, 1, wx.EXPAND, 0)
            if panel_info.start_panel:
                new_panel.Show()
            else:
                new_panel.Hide()
            self._panels[panel_name] = new_panel
        # set app icon
        # icon = wx.Icon()
        # icon.CopyFromBitmap(wx.Bitmap("shape_arrangements.ico", wx.BITMAP_TYPE_ANY))
        # self.SetIcon(icon)
        self._base_title = self.GetTitle()
        self._panel_name_stack = list()

    def get_panel(self, panel_name: str):
        """Get panel instance.

        Args:
            panel_name (str): name of panel

        Returns:
            wx.Panel: found panel instance
        """
        return self._panels[panel_name]

    def get_panel_name(self, panel: wx.Panel):
        """TODO."""
        res = list(filter(lambda pp: pp[1] is panel, self._panels.items()))
        if not res:
            raise ValueError(f"panel object {panel} cannot be found")
        if len(res) > 1:
            raise ValueError(f"panel object {panel} can be found more times")
        return res[0][0]
